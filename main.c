/* C Standard library */
#include <stdio.h>
#include <stdlib.h>

/* XDCtools files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC26XX.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <ti/drivers/UART.h>


/* Board Header files */
#include "Board.h"
#include "wireless/comm_lib.h"


#define STACKSIZE 2048
Char networkTaskStack[STACKSIZE];

static PIN_Handle button1Handle;
static PIN_State button1State;
PIN_Config button1Config[] = {
   Board_BUTTON1  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
   PIN_TERMINATE // Asetustaulukko lopetetaan aina tällä vakiolla
};


void button1Fxn(PIN_Handle handle, PIN_Id pinId) {

    char payload[16] = "ping";
    Send6LoWPAN(IEEE80154_SERVER_ADDR, payload, strlen(payload));
    StartReceive6LoWPAN();
}

Void networkTaskFxn(UArg arg0, UArg arg1) {
   char payload[80]; // Read Buffer
   uint16_t senderAddr;

   int32_t result = StartReceive6LoWPAN();
   if(result != true) {
      System_abort("Wireless receive start failed");
   }

   while (1){
       System_printf("networkTask\n");
       System_flush();
       if (GetRXFlag()) {

          // Tyhjennet��n puskuri (ettei sinne j��nyt edellisen viestin j�mi�)
          memset(payload,0,80);
          // Luetaan viesti puskuriin payload
          Receive6LoWPAN(&senderAddr, payload, 80);
          // Tulostetaan vastaanotettu viesti konsoli-ikkunaan
          System_printf(payload);
          System_flush();

       }

       // Once per second, you can modify this
       Task_sleep(1000000 / Clock_tickPeriod);
   }
}

Int main(void) {

    Task_Handle networkTaskHandle;
    Task_Params networkTaskParams;
    // Initialize board
    Board_initGeneral();
    Init6LoWPAN();
    button1Handle = PIN_open(&button1State, button1Config);
    if(!button1Handle) {
       System_abort("Error initializing button 1 pins\n");
    }
    if (PIN_registerIntCb(button1Handle, &button1Fxn) != 0) {
            System_abort("Error registering button 1 callback function");
    }
    Task_Params_init(&networkTaskParams);
    networkTaskParams.stackSize = STACKSIZE;
    networkTaskParams.stack = &networkTaskStack;
    networkTaskHandle = Task_create(networkTaskFxn, &networkTaskParams, NULL);
    if (networkTaskHandle == NULL) {
       System_abort("Task create failed!");
    }
    /* Sanity check */
    System_printf("Starting app!\n");
    System_flush();
    /* Start BIOS */
    BIOS_start();

    return (0);

}

